import React, { useState } from "react";
import todosList from "./todos.json";
import TodoList from './components/todolist/TodoList'
import { v4 as uuidv4 } from 'uuid';

function App () {
  const [todos, setTodos] = useState(todosList) // <---sets initial state

  const handleSubmit = (e) => {
    if (e.key === 'Enter' && e.target.value.trim() !== ''){

      let newTodo = {
        'id': uuidv4(),
        'completed': false,
        'title': e.target.value.trim(),
        'userId': 1
      }

      let updatedTodos = [...todos]
      updatedTodos.push(newTodo)

      setTodos((t) => 
       [newTodo, ...t]
      )

      e.target.value = ''
    }
  }

  const handleCompleted = (id) => {
    const newTodos = todos.map(
      todoItem => {
        if(todoItem.id === id) {
          todoItem.completed = !todoItem.completed
        }
        return todoItem
      }
    )
    setTodos(() => {
      return newTodos
    })
  } 
  
  const handleDelete = (id) => {
    const newTodos = todos.filter(
      todoItem => todoItem.id !== id
    )
    setTodos(() => {
      return newTodos
    })
  }

  const handleClearCompleted = () => {
    const newTodos = todos.filter(
      completedItems => completedItems.completed === false       
    )
    setTodos(() => {
      return newTodos
    })
  }

  return (
    <section className="todoapp">
      <header className="header">
        <h1>todos</h1>
        <input onKeyDown={handleSubmit} type='text' className="new-todo" placeholder="What needs to be done?" autoFocus />
      </header>
      <TodoList todos={todos} handleCompleted={handleCompleted} handleDelete={handleDelete} />
      <footer className="footer">
        <span className="todo-count">
          <strong>0</strong> item(s) left
        </span>
        <button onClick={handleClearCompleted} className="clear-completed">Clear completed</button>
      </footer>
    </section>
  );
}


export default App;
