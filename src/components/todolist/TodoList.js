import React from 'react'
import TodoItem from '../todoitem/TodoItem'

function TodoList (props) {

    return (
      <section className="main">
        <ul className="todo-list">
          {props.todos.map((todo) => (
            <TodoItem handleDelete={props.handleDelete} handleCompleted={props.handleCompleted} title={todo.title} completed={todo.completed} key={todo.id} id={todo.id} />
          ))}
        </ul>
      </section>
    );
}

export default TodoList