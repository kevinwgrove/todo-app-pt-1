import React from 'react'

function TodoItem (props) {

    return (
        <li key={props.id} className={props.completed ? "completed" : ""}>
        <div className="view">
            <input onChange={() => props.handleCompleted(props.id)} className="toggle" type="checkbox" checked={props.completed} />
            <label>{props.title}</label>
            <button className="destroy" onClick={() => props.handleDelete(props.id)} />
        </div>
        </li>
    );
}

export default TodoItem
